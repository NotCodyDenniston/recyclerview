package com.example.recyclerview.viewmodel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.R
import com.example.recyclerview.model.RecyclerRepo
import kotlinx.android.synthetic.main.list_item.view.tvTitle

class ListAdapter: RecyclerView.Adapter<ListAdapter.ListViewHolder>() {
    val list = listOf(
        "afadsf",
        "asdfafa",
        "fdasgtega",
        "qwtqwerw",
        "wteyhsdhgs",
        "?≥≤√∫√åƒåƒ∂å",
        "å†©ƒagå®©†å",
        "œ´´∑´´∑∫©≈√≈Ω˜˜∫√å≈ΩÏ",
        "¡∞¡£¢∞Á§Á∑©Ó∑§´∞ÅÅ√…",
        "QRQ†ÁÁÓÁ¨∏ÍÎ©Í",
        "Å≈Ç√ı©ÏÎ´®†ÁÓ©ı˜",
        "–“∏¶Á…ÁÒ",
        "Aå®©åƒÅ"
    )

    inner class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.itemView.apply {
            tvTitle.text = list[position]
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }


}