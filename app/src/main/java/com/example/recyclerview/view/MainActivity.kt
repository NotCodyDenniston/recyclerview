package com.example.recyclerview.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerview.R
import com.example.recyclerview.viewmodel.ListAdapter
import kotlinx.android.synthetic.main.activity_main.recycler_view

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler_view.adapter = ListAdapter()
        recycler_view.layoutManager = LinearLayoutManager(this)
    }
}

